import 'bootstrap-loader';
import 'font-awesome-sass-loader';
import './app.scss';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
    <p>Hello, world!</p>,
    document.getElementById('root')
);

