const path = require('path');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');
const isProd = process.env.NODE_ENV == 'production';
const cssExtractText = ExtractTextPlugin.extract(
    {
        fallback: 'style-loader',
        use: ['css-loader', 'postcss-loader', 'resolve-url-loader']
    }
);

const scssExtractText = ExtractTextPlugin.extract(
    {
        fallback: 'style-loader',
        use: ['css-loader', 'postcss-loader', 'resolve-url-loader', 'sass-loader?sourceMap']
    }
);
const bootstrapEntryPoints = require('./webpack.bootstrap.config.js');
const bootstartConfig = isProd ? bootstrapEntryPoints.prod : bootstrapEntryPoints.dev;
const glob = require('glob');
const PurifyCSSPlugin = require('purifycss-webpack');

const entry = {
    bootstrap: bootstartConfig,
    app: './src/app.js'
};

console.log('isProd:', isProd, process.env.NODE_ENV);

module.exports = {
    entry: entry,
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Rock Band',
            template: './src/index.html',
            minify: {
                collapseWhitespace: true
            },
            hash: true
        }),
        new ExtractTextPlugin('css/[name].css'),
        new webpack.LoaderOptionsPlugin({
            postcss: [autoprefixer]
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            Tether: "tether",
            "window.Tether": "tether",
            Alert: "exports-loader?Alert!bootstrap/js/dist/alert",
            Button: "exports-loader?Button!bootstrap/js/dist/button",
            Carousel: "exports-loader?Carousel!bootstrap/js/dist/carousel",
            Collapse: "exports-loader?Collapse!bootstrap/js/dist/collapse",
            Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
            Modal: "exports-loader?Modal!bootstrap/js/dist/modal",
            Popover: "exports-loader?Popover!bootstrap/js/dist/popover",
            Scrollspy: "exports-loader?Scrollspy!bootstrap/js/dist/scrollspy",
            Tab: "exports-loader?Tab!bootstrap/js/dist/tab",
            Tooltip: "exports-loader?Tooltip!bootstrap/js/dist/tooltip",
            Util: "exports-loader?Util!bootstrap/js/dist/util",
        }),
        new PurifyCSSPlugin({
            paths: glob.sync(path.join(__dirname, 'src/*.html')),
        })
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: cssExtractText,
            },
            {
                test: /\.scss$/,
                use: scssExtractText
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10000,
                            mimetype: 'application/font-woff',
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            },
            {
                test: /bootstrap[\/\\]dist[\/\\]js[\/\\]umd[\/\\]/,
                loader: 'imports-loader?jQuery=jquery'
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader?hash=sha512&digest=hex&name=[hash].[ext]&outputPath=images/',
                    'image-webpack-loader'
                ]
            },
        ],
    },
    devServer: {
        hot: true,
        contentBase: path.resolve(__dirname, 'dist'),
        stats: 'errors-only',
        open: true,
        publicPath: '/'
    }
};